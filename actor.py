import csv
import os
import requests
import sqlite3
import sys
from random import randint
from time import sleep

DB_FILE = 'sqlite3.db'
INPUT_FILE = "input.csv"
OUTPUT_FILE = "output.csv"

DEVELOPMENT = False

api_root = "https://api.tvmaze.com/"
api_ppl = api_root + "people/{}"
api_ppl_search = api_root + "search/people?q={}"
api_pax_shows = api_ppl + "/castcredits?embed=show"
api_show_detail = api_root + "shows/{}"
api_show_episodes = api_show_detail + "/episodes?specials=1#"

API_REQUEST_TIMEOUT = 5  # seconds

DB_FIELDS = {
    'actor': 'id integer PRIMARY KEY, tvm_id integer NOT NULL, name text, comment text',
    'show': 'id integer PRIMARY KEY, tvm_id integer NOT NULL, name text, language text, summary text, genres text, '
            'seasons integer, episodes text, vtime decimal, comment text',
    'cast': 'id integer PRIMARY KEY, actor_tvm_id integer NOT NULL, show_tvm_id integer NOT NULL'
}

CSV_HEADER = ['actor_name', 'show_name', 'language', 'summary', 'genres', 'number of seasons',
              '[' + 'number of episodes' + ']', 'total view time (hours)']

if not DEVELOPMENT:
    sys.stdout = open(os.devnull, 'w')


def db_init(db_file='electric8.db'):
    try:
        db = sqlite3.Connection(db_file)
        cursor = db.cursor()
        return cursor
    except:
        print("DB-Error: init()")
        return None


def check_tbl(tbl_name):
    # return 0 if already exists
    # return 1 if created
    # return -1 in case of error
    try:
        cursor.execute('''SELECT count(name) FROM sqlite_master WHERE type='table' AND name=?''', (tbl_name,))
    except TypeError:
        print("Table `{}` doesn't exist".format(tbl_name))
        try:
            if cursor.fetchone()[0] < 1:  # if the count is <1, then create table
                create_stmt = '''CREATE TABLE {0} ({1})'''.format(tbl_name, DB_FIELDS[tbl_name])
                print(create_stmt)
                cursor.execute(create_stmt)
                print("Table `{}` created with no data".format(tbl_name))
                return 1
        except:
            print('Error handling DB operation. Table --> {}'.format(tbl_name))
            return -1


def check_db(cursor):
    #  check for tables & create if not yet there
    create_table_actor = """CREATE TABLE IF NOT EXISTS actors (
        id integer PRIMARY KEY AUTOINCREMENT, 
        tvm_id integer NOT NULL, 
        name text, 
        comment text
        )
    """

    create_table_shows = """CREATE TABLE IF NOT EXISTS shows (
        id integer PRIMARY KEY AUTOINCREMENT, 
        tvm_id integer NOT NULL, 
        name text, language text, summary text, genres text,
        seasons integer, episodes text, vtime decimal, comment text
        )
    """

    create_table_casts = """CREATE TABLE IF NOT EXISTS casts (
        id integer PRIMARY KEY AUTOINCREMENT, 
        actor_tvm_id integer NOT NULL, 
        show_tvm_id integer NOT NULL,
        UNIQUE(actor_tvm_id, show_tvm_id)
        )
    """

    current_table = None
    try:
        current_table = 'actors'
        cursor.execute(create_table_actor)
        current_table = 'shows'
        cursor.execute(create_table_shows)
        current_table = 'casts'
        cursor.execute(create_table_casts)
        return True
    except:
        raise IOError("DB Error: access/create db-table(s): {}".format(current_table))


def random_delay():
    delay = randint(0, 5)
    print("To avoid API throttling, intentionally delaying for {} seconds".format(delay))
    sleep(delay)


def api_fetch(name, url):
    print('Fetching {0} via API : {1}'.format(name, url))
    try:
        random_delay()  # to avoid API-throttling by TVMaze host server
        r = requests.get(url, timeout=API_REQUEST_TIMEOUT)
        status_code = r.status_code if r.status_code else None
        payload = r.json() if r.json() else None
        return status_code, payload
    except ValueError:
        print("{}: Invalid JSON received from remote API".format(name))
    except:
        print("Error fetching `{}` from API".format(name))


def lookup_actor(name: str):
    print(name)
    actor_idx = None
    try:
        cursor.execute("""SELECT tvm_id FROM actors WHERE name=? LIMIT 1""", (name,))
        actor_idx = cursor.fetchone()[0]
        print(actor_idx)
        if actor_idx == 0:
            print("No actor found reliably, with the provided name('{}')".format(name))
            return None
        return actor_idx
    except:
        print('DB Error')

    if actor_idx == None:
        print("Actor-ID couldn't be fetched from DB. Fetching via API")
        # fetch from API
        status_code, payload = api_fetch('lookup_actor', api_ppl_search.format('+'.join(name.split())))
        # print("api_ppl_search API Response: {}".format(status_code))

        if (status_code != 200):
            return None

        # filter for noise
        if payload[0]['score'] > 60:
            actor_idx = payload[0]['person']['id']

    if actor_idx:
        print(actor_idx)
        cursor.execute("""INSERT INTO actors (tvm_id, name) VALUES (?, ?)""", (actor_idx, name))
        return actor_idx
    else:
        print("No actor found reliably, with the provided name('{}')".format(name))
        cursor.execute("""INSERT INTO actors (tvm_id, name, comment) VALUES (0, ?, 'could not be found reliably')""",
                       (name,))
        return None


def lookup_for_top_shows(actor_id):
    shows = []
    show_ids = []

    try:
        cursor.execute("""SELECT show_tvm_id FROM casts WHERE actor_tvm_id = ? LIMIT 3""", (actor_id,))
        shows = cursor.fetchall()
        show_ids = [_[0] for _ in shows]
        # print(show_ids)
        if show_ids:
            return show_ids
    except:
        print("DB error")

    if not len(show_ids):
        # fetch from API
        status_code, payload = api_fetch('lookup_for_top_shows', api_pax_shows.format(actor_id))
        print("api_ppl_search API Response: {}".format(status_code))

        if not ((status_code == 200) and payload):
            return None

        shows_raw = [x for x in payload]
        shows, shows_without_rating = [], []
        for x in shows_raw:
            if x['_embedded']['show']['rating']['average']:  # filter out shows without rating
                shows.append(x['_embedded']['show'])
            else:
                shows_without_rating.append(x['_embedded']['show'])

        # find out top-rated shows
        sorted_shows = sorted(shows, key=lambda show: show['rating']['average'], reverse=True)
        # print(sorted_shows[:3])
        # print([(show['id'], show['rating']['average']) for show in sorted_shows[:3]])
        # print([show for show in sorted_shows[:3]])

        for api_show in sorted_shows[:3]:
            show_ids.append(api_show['id'])

            # insert (actor_id, show_id) into `casts` table
            try:
                cursor.execute(
                    """INSERT OR IGNORE INTO casts 
                    (actor_tvm_id, show_tvm_id) VALUES(?, ?)""", (actor_id, api_show['id']))
            except:
                print("DB Error: writing actor-show relationship table")

    return show_ids


def fetch_episode_details(show_id):
    episodes = None
    status_code, payload = api_fetch("Episodes-details", api_show_episodes.format(show_id))
    if status_code == 200:
        episodes = payload
    # print(payload)

    seasons = {}
    total_vtime: int = 0
    seasons = {}
    for episode in episodes:
        if episode['season']:  # count episodes using dict `seasons`
            season_number = episode['season']
            seasons[str(season_number)] = int(seasons.get(str(season_number), '0')) + 1
        if episode['runtime']:
            total_vtime += int(episode['runtime'])
    # print(seasons)
    episodes_count = sorted([(int(x), seasons[x]) for x in seasons.keys()])

    # returning only the episodes-count as the list order implies the season number
    return [x[1] for x in episodes_count], total_vtime


def get_show_detail(show_id):
    try:
        cursor.execute("""SELECT tvm_id, name, language, summary, genres, seasons, episodes, vtime, comment FROM shows 
                            WHERE tvm_id = ? LIMIT 1""",
                       (show_id,)
                       )
        tvm_id, name, language, summary, genres, seasons, episodes, vtime, comment, = cursor.fetchone()
        # print(tvm_id, name, language, summary, genres, seasons, episodes, vtime, comment)
        return tvm_id, name, language, summary, genres, seasons, episodes, vtime, comment
    except:
        # fetch from API
        status_code, api_show = api_fetch('get_show_detail', api_show_detail.format(show_id))

        if status_code == 200 and api_show:

            tvm_id = api_show['id']
            name = api_show['name']
            language = api_show['language'] if api_show['language'] else ''
            genres = ','.join(api_show['genres']) if api_show['genres'] else ''
            summary = api_show['summary'] if api_show['summary'] else ''
            episodes_count, vtime = fetch_episode_details(show_id)  # return list of episode-count per season
            seasons = len(episodes_count)
            episodes = ','.join([str(x) for x in episodes_count])
            comment = ''
            # print(tvm_id, name, language, summary, genres, seasons, episodes, vtime, comment)

            # insert show-details into `shows` table
            try:
                cursor.execute("""INSERT INTO shows 
                                   (tvm_id, name, language, summary, genres, seasons, episodes, vtime, comment) 
                                   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)""",
                               (tvm_id, name, language, summary, genres, seasons, episodes, vtime, comment))
            except:
                print("`get_show_detail()` got DB error to insert show details")

            return tvm_id, name, language, summary, genres, seasons, episodes, vtime, comment


if __name__ == "__main__":
    db = sqlite3.Connection(DB_FILE, isolation_level=None)  # initialized with "auto-commit
    cursor = db.cursor()
    check_db(cursor)

    with open(INPUT_FILE, mode='r', encoding="utf-8") as fin, \
            open(OUTPUT_FILE, mode='wt', encoding="utf-8", newline='') as fout:
        csv_reader = csv.reader(fin, quoting=csv.QUOTE_NONE)

        csv_writer = csv.writer(fout, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        csv_writer.writerow(CSV_HEADER)

        next(fin)  # skips the first line
        for actor_name in csv_reader:
            actor_name = ' '.join(actor_name)
            actor_id = lookup_actor(actor_name)

            # lookup for the top shows. Shows with no ranking will be ignored
            if actor_id:
                show_ids = lookup_for_top_shows(actor_id)
                print(show_ids)

                if show_ids:
                    for show_id in show_ids:
                        tvm_id, show_name, language, summary, genres, seasons, episodes, vtime, comment = get_show_detail(
                            show_id)
                        # output data
                        print(actor_name, show_name, language, summary, genres, seasons, '[' + episodes + ']',
                              vtime / 60, sep=',')
                        csv_writer.writerow(
                            [actor_name, show_name, language, summary, genres, seasons, '[' + episodes + ']',
                             vtime / 60])
    if cursor:
        cursor.close()
    if db:
        db.close()

    print("The output file is: {}".format(OUTPUT_FILE))
