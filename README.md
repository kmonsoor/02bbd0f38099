Preparation
===========
The solution needs `Python 3.7 (or higher)` execution environment. This can installed from [here](https://www.python.org/downloads/).

Once installed, you can check it using this command.

`$ python3 --version`

`Python 3.8.2  (note: sample output)` 

Now, clone the solution folder from GitLab.com

`$ git clone https://gitlab.com/kmonsoor/02bbd0f38099`

Execution
=========
Once the "cloning" is done, navigate to the downloaded solution folder.

`$ cd 02bbd0f38099`

Then, please install the dependency libraries (listed in the `requirements.txt` file) using `pip3` utility that comes with python.

`$ python3 -m pip install --upgrade pip`

`$ pip3 install -r requirements.txt` 


Then, run the solution using the command

```$ python3 actor.py```

Notes
=====
* TV shows with no ranking are discarded from the results, hence some actors might be showed with fewer than 3 TV shows.
* The number of episodes in individual seasons are grouped together, in the output row, as a "list" e.g. `[13, 10, 10]` with the season-number as implied from the sequence. 
For example, `[13, 10, 10]` implies that the TV show has 13 episodes in the first season, and 10 episodes each for the 2nd and the 3rd season.
* The included `sqlite3.db` file is used as the persistent storage, i.e. relational database for the solution. Upon cloning from the repository, it's in barebone state.
During the first execution of `actor.py`, the initialization of the database (e.g. creating the tables, etc.) takes place.
* Upon execution, the output file is saved as `output.csv` in the same folder. 
* The logging, updates during the execution, is disabled for submission. 
To see the progress realtime, please update the value of the variable `DEVELOPMENT` from `False` to `True` on the file, `actor.py` (line: 13).


Assumptions
===========
* There's only one actor with the same name
* In case of reliable search, the intended actor returns with a "score" of 60 or more. Any other search results are discarded. 
* Each individual actor & TV-show has only one and unique ID on TVMaze host
* The "input" file is named as `input.csv` and using UTF-8 text encoding.
